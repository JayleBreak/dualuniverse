local AspectRatio= 1.67        --export: Width to height ratio (greater than 1)
local Material1  = '#ff0000'   --export: Material color
local Material2  = '#00ff00'   --export: Material color
local Material3  = '#0000ff'   --export: Material color
local Material4  = '#ffff00'   --export: Material color
local Material5  = '#ff00ff'   --export: Material color
local Material6  = '#00ffff'   --export: Material color
local Material7  = '#efc8c8'   --export: Material color

local Colors     = { Material1, Material2, Material3, Material4,
                     Material5, Material6, Material7 }

local utils = require('cpml.utils')
local round = utils.round

local function centerText(ulx, uly, lrx, lry, label, class)
    return string.format([[
<text x="%.1f" y="%.1f" font-size="%.1f" class="%s" dominant-baseline="middle" text-anchor="middle">%s</text>]],
        (ulx+lrx)/2, (uly+lry)/2, (lry-uly)/2, class or 'T', label)
end

local PushButton     = {}
PushButton.__index   = PushButton

function PushButton.New(ulx, uly, lrx, lry, fill, label, callback)
    -- callback is nil for non-responsive labels
    return setmetatable({ ulx      = ulx,
                          uly      = uly,
                          lrx      = lrx,
                          lry      = lry,
                          fill     = fill,
                          modifier = callback and 'L' or 'I',
                          label    = label,
                          callback = callback }, PushButton)
end

function PushButton:draw()
    local class = self.fill
    local label = self.label

    if type(label) == 'table' or type(label) == 'function' then
        label = label(self) -- can change the modifier
    end

    if self.modifier and self.modifier ~= '' then
        class = class .. ' ' .. self.modifier
    end
    local text  = label and
                  centerText(self.ulx,self.uly,self.lrx,self.lry,label) or ''
    return string.format([[
<polygon class="%s" points="%.0f,%.0f %.0f,%.0f %.0f,%.0f %.0f,%.0f"/>%s]],
      class, self.ulx, self.uly, self.lrx, self.uly, self.lrx, self.lry,
      self.ulx, self.lry, text)
end

function PushButton:onClick(button, x, y)
    if button == 1 and self.callback then -- LMB only
        if  x >= self.ulx and x <= self.lrx and
            y >= self.uly and y <= self.lry then
            local modifier = self.modifier
            self.modifier = 'S'
            return
                function(down, button, x, y)
                    if button == 1 and not down and
                       x >= self.ulx and x <= self.lrx and
                       y >= self.uly and y <= self.lry then
                        self.callback(self, modifier)
                    else
                        self.modifier = modifier
                    end
                end
        end
    end
    return nil
end

local HelpPages = {
[[
<text y="5">
 <tspan x="50" style="font-size:.5vw" dominant-baseline="middle" text-anchor="middle">2D+ Voxel Planner and Builder</tspan>
 <tspan x="50" dy="1em" style="font-size:.4vw" dominant-baseline="middle" text-anchor="middle">By: JayleBreak</tspan>
</text>
<text y="8">
 <tspan x="5" dy="3em" style="font-size:.4vw">The Plan Designer</tspan>
 <tspan x="5" dy="2em">The application allows up to 3 different users to create voxel plans</tspan>
 <tspan x="5" dy="1em">which are saved in a databank. The least recently used plan is discarded</tspan>
 <tspan x="5" dy="1em">if additional users access the designer.</tspan>
 <tspan x="5" dy="2em">The voxel plan consists of up to 12 layers of 2D slices. Each layer is</tspan>
 <tspan x="5" dy="1em">a 15 by 15 array of voxels broken into 4 "pages" of which one is shown.</tspan>
 <tspan x="5" dy="1em">The current "page" is altered using mouse actions:</tspan>
 <tspan x="7" dy="2em">Left Mouse Button Click:</tspan>
 <tspan x="7" dy="1em">Change the material of the selected voxel.</tspan>
 <tspan x="7" dy="2em">Left Shift + Left Mouse Button Drag:</tspan>
 <tspan x="7" dy="1em">Drag closest vertext to a new position.</tspan>
 <tspan x="7" dy="2em">Left ALT + Left Mouse Button Click:</tspan>
 <tspan x="7" dy="1em">Clear the material of the selected voxel.</tspan>
 <tspan x="7" dy="2em">Middle Mouse Button Click:</tspan>
 <tspan x="7" dy="1em">Undo the most recent change.</tspan>
 <tspan x="7" dy="2em">Held Left Shift</tspan>
 <tspan x="7" dy="1em">Show the coordinates of the mouse position.</tspan>
 <tspan x="5" dy="2em">The panel to the left of the "page" has buttons for 7 different materials,</tspan>
 <tspan x="5" dy="1em">a "Build" button, and a "Reset" button.  The "Reset" button clears all</tspan>
 <tspan x="5" dy="1em">layers and cannot be undone.</tspan>
 <tspan x="5" dy="2em">The panel to the right of the "page" has buttons to change the current layer,</tspan>
 <tspan x="5" dy="1em">change the current page, "Undo" and "Redo" actions, and "Clear" which clears</tspan>
 <tspan x="5" dy="1em">the current layer and CAN be undone.</tspan>
 <tspan x="5" dy="2em">Changing the current layer, current page, or logging out will save the changes</tspan>
 <tspan x="5" dy="1em">to the current layer.</tspan>
</text>
]],
[[
<text y="5">
 <tspan x="5" style="font-size:.4vw">The Plan Builder</tspan>
 <tspan x="5" dy="2em">Pressing the "Build" button starts the process for assembling voxels according to</tspan>
 <tspan x="5" dy="1.1em">the current layer's plan. After pressing the "Build" button and exiting the chair,</tspan>
 <tspan x="5" dy="1.1em">the user should enter DU's build mode (pressing 'B'). Note that when in build mode</tspan>
 <tspan x="5" dy="1.1em">you will not be able to interact with the screen which will be showing your</tspan>
 <tspan x="5" dy="1.1em">progress in the build process.</tspan>
 <tspan x="5" dy="2em">The build process consists of repeatedly selecting and copying a voxel from the</tspan>
 <tspan x="5" dy="1.1em">voxel board, and pasting them on the opposing assembly board. The voxel to be</tspan>
 <tspan x="5" dy="1.1em">copied and the place to paste it are indicated by arrow pointers.</tspan>
 <tspan x="5" dy="2em">When pasting, the material to be selected from the quick access menu is indicated</tspan>
 <tspan x="5" dy="1.1em">by a number. If the number is "0", then use the copied voxels original material.</tspan>
 <tspan x="5" dy="1.1em">A voxel with material "0" is shaping voxel and not part of the final build.</tspan>
 <tspan x="5" dy="2em">After pasting the selected voxel, the button on the pedestal below the assembly</tspan>
 <tspan x="5" dy="1.1em">board should be activated to trigger the advance to the next voxel to be copied.</tspan>
 <tspan x="5" dy="2em">When the last voxel has been placed the arrows will no long be displayed, and the</tspan>
 <tspan x="5" dy="1.1em">screen will be set to display the current layer's plan. The final step is to copy</tspan>
 <tspan x="5" dy="1.1em">the assembled layer to a final assembly area where multiple layers will be stacked.</tspan>
 <tspan x="5" dy="2em">When doing this the shaping voxels should NOT be copied. Sometimes to copy offset</tspan>
 <tspan x="5" dy="1.1em">voxels, a "handle will be needed. There are red voxels on the edges of the assembly</tspan>
 <tspan x="5" dy="1.1em">board that can be copied and placed as handles. Note that deleting the shaping</tspan>
 <tspan x="5" dy="1.1em">voxels will usually result in undesired distortion at this time.</tspan>
</text>
]],
}

local LASTPG = #HelpPages

local TextCanvas     = {}
TextCanvas.__index   = TextCanvas

function TextCanvas.New(ulx, uly, helpText)
    -- callback is nil for non-responsive labels
    return setmetatable({ x = ulx, y = uly, helpText = helpText }, TextCanvas)
end

function TextCanvas:draw()
    local text = self.helpText

    if type(text) == 'table' or type(text) == 'function' then
        text = text() -- can change the modifier
    end
    return text
end

function TextCanvas:onClick(button, x, y)
    return nil
end

local Panel   = {}
Panel.__index = Panel

function Panel.New(xoffset, sWidth, sHeight, width, height)
    return setmetatable({ xoffset = xoffset, sWidth = sWidth,
                          sHeight = sHeight, width  = width, height = height,
                          widgets = {} }, Panel)
end

function Panel:onClick(button, x, y)
    local fn

    if x > self.xoffset and x < self.xoffset+self.sWidth then
        fn = function() end
        local xs, ys = self:scaleXY(x, y)

        for _, widget in ipairs(self.widgets) do
            local fnw = widget:onClick(button, xs, ys)
            if fnw then
                fn = function(down, button, x, y)
                        fnw(down, button, self:scaleXY(x, y))
                     end
                break
            end
        end
    end
    return fn
end

function Panel:draw()
    local svgList = {}

    for _, widget in ipairs(self.widgets) do
        table.insert(svgList, widget:draw())
    end
    return string.format([[
<g transform="translate(%.0f,0) scale(%.2f,%.2f)">
%s
</g>]], self.xoffset, self.sWidth/self.width, self.sHeight/self.height,
        table.concat(svgList, '\n'))
end

function Panel:addWidget(widget)
    table.insert(self.widgets, widget)
    widget.id = #self.widgets
    return widget
end

function Panel:scaleXY(x, y)
    local xs = (x-self.xoffset)*self.width/self.sWidth
    local ys = y*self.height/self.sHeight
    return xs, ys
end

local function pushLabel(tbl, label)
    return
        function(button)
            button.modifier = next(tbl) and 'MB' or 'D'
            return label
        end
end

local Screen   = {}
Screen.__index = Screen

function Screen.New(stack, printer)
    local panelSize = stack.DisplaySize
    local sideWidth = (AspectRatio-1)*panelSize/2
    local screen    = setmetatable({stack         = stack,
                                    material      = 1,
                                    printer       = printer,
                                    helpPanels    = {},
                                    layerPanels   = {},
                                    printerPanels = {}}, Screen)

    local materials = {}
    screen:makeLayerPanels(materials)
    screen:makePrinterPanels()
    screen:makeHelpPanel()

    local width  = math.ceil(AspectRatio*stack.DisplaySize)
    local height = stack.DisplaySize

    screen.helpStyle = string.format([[
<svg xmlns="http://www.w3.org/2000/svg" width="100%%%%" height="100%%%%"
preserveAspectRatio="xMinYMin meet" viewBox="0 0 %s %s"
style="background-color:#202020">
<style>
    svg {
        font-family: Bank;
        fill: white;
        font-size: .2vw;
    }
    .L {stroke:black;stroke-width:.1;stroke-opacity:.8;}
    .T {font-family: monospace; font-weight:bold; stroke:black; }
    .MB{fill:#f0f0f0;}
    .MD{fill:#202020;}
    .i { font-style: italic; }
</style>
%%s
</svg>]], width, height)

    screen.layerStyle = string.format([[
<svg xmlns="http://www.w3.org/2000/svg"
width="100%%%%" height="100%%%%"
preserveAspectRatio="xMinYMin meet"
viewBox="0 0 %s %s"
style="background-color:#a8a8a8">
<style>
.T{font-family:monospace; font-weight:bold;}
.I{stroke-opacity:0;}
.L{stroke:black;stroke-width:.1;stroke-opacity:.8;}
.S{stroke:black;stroke-width:.4;stroke-opacity:1;}
.W{fill-opacity:.5;}
.D{fill:#606060;}
.MB{fill:#f0f0f0;}
.M0{fill:none;stroke-width:.1}
%s
</style>
%%s
</svg>]], width, height, table.concat(materials, '\n')) -- style
    screen.svgFormat = screen.layerStyle

    screen.panels = screen.layerPanels
    return screen
end

function Screen.PolygonSvg(svgList, polygons, opacity, override)
    for _, polygon in ipairs(polygons) do
        local vertices, vox = table.unpack(polygon)
        local points   = {}
        local material = override or vox:getMaterial()
        --[[local id=string.format('"R%sC%s"',vox.rowIndex,vox.columnIndex)--]]

        for _,v in ipairs(vertices) do
            table.insert(points,
                         string.format('%.0f,%.0f', v[1], v[2]))
        end
        local class  = string.format('M%s %s', material or 0, opacity)
        local svg    = string.format('<polygon class="%s" points="%s"/>',
                                     class,
                                     table.concat(points, ' '))

        table.insert(svgList, svg)
    end
end

function Screen:onClick(down, button, x, y)
    if self.upCb then
        self.upCb(down, button, self:scaleXY(x, y))
        self.upCb = nil
    end

    if down then
        for _, panel in ipairs(self.panels) do
            self.upCb = panel:onClick(button, self:scaleXY(x, y))

            if self.upCb then return end
        end
    end
end

function Screen:changeMaterial(index)
    local previous = self.stack:changeMaterial(index)
    self.layerPanels[1].widgets[previous].modifier = 'L'
    self.layerPanels[1].widgets[index].modifier    = 'S'
end

function Screen:doPrint(btn)
    btn.modifier = 'L'
    self.stack:saveData()
    if self.printer:init(self.stack:layer(),
                         function() self:endPrint() end) then
        self.panels = self.printerPanels
    else
        printf('No voxels set in current layer.')
    end
end

function Screen:draw()
    local panelSvg = {}

    for _, panel in ipairs(self.panels) do
        table.insert(panelSvg, panel:draw())
    end

    return string.format(self.svgFormat, table.concat(panelSvg, '\n'))
end

function Screen:endPrint()
    self.printer:endPrint()
    self.panels = self.layerPanels
end

function Screen:logout()
    self.stack:saveData()
    control.exit()
end

function Screen:makeHelpPanel()
    local function chgPage(screen, delta)
        if delta then
            self.helpPage = screen.helpPage + delta
            if self.helpPage < 1 then screen.helpPage = 1 end
            if self.helpPage > LASTPG then screen.helpPage = LASTPG end
        else
            self.helpPage  = nil
            self.panels    = screen.layerPanels
            self.svgFormat = screen.layerStyle
        end
    end
    local function showPrev(button)
        if self.helpPage == 1 then
            button.modifier = 'MD'
            return ''
        end
        button.modifier = 'MB'
        return '&lt;'
    end
    local function showNext(button)
        if self.helpPage == LASTPG then
            button.modifier = 'MD'
            return ''
        end
        button.modifier = 'MB'
        return '&gt;'
    end
    local height = self.stack.DisplaySize
    local width  = math.ceil(AspectRatio*height)
    local panel  = Panel.New(0, width, height, 100, 100)
    table.insert(self.helpPanels, panel) -- full screen
    panel:addWidget(PushButton.New(39,91,45,96,'', showPrev,
                    function() chgPage(self,-1) end))
    panel:addWidget(PushButton.New(47,91,53,96,'MB', '^',
                    function() chgPage(self) end))
    panel:addWidget(PushButton.New(55,91,61,96,'', showNext,
                    function() chgPage(self, 1) end))
    panel:addWidget(TextCanvas.New(10, 0,
                    function() return HelpPages[self.helpPage] end))
end

function Screen:makeLayerPanels(materials)
    local stack     = self.stack
    local panelSize = stack.DisplaySize
    local sideWidth = (AspectRatio-1)*panelSize/2

    local panel     = Panel.New(0, sideWidth, panelSize, 24, 100)

    for i,color in ipairs(Colors) do
        local fill = string.format('M%s', i)
        local push = function() self:changeMaterial(i) end
        panel:addWidget(
              PushButton.New(5, 8*i, 19, 8*i+6, fill, i, push)).modifier = 'L'

        local style = string.format('.M%s{fill:%s}', i, color)
        table.insert(materials, style)
    end
    panel.widgets[1].modifier = 'S'
    stack:changeMaterial(1)
    panel:addWidget(PushButton.New(4, 67, 20, 74, 'MB', 'Build',
                    function(btn) self:doPrint(btn) end))
    panel:addWidget(PushButton.New(4, 93, 20, 99, 'MB', 'Reset',
                    function() stack:reset() end))
    table.insert(self.layerPanels, panel) -- left

    panel = Panel.New(sideWidth, panelSize, panelSize, panelSize, panelSize)
    panel:addWidget(stack)
    table.insert(self.layerPanels, panel) -- middle

    panel = Panel.New(panelSize+sideWidth, sideWidth, panelSize, 24, 100)
    panel:addWidget(PushButton.New(10, 5, 17, 10, 'MB',
                    function() return stack:cursor() end)) -- show current layer
    panel:addWidget(PushButton.New(5, 5, 9, 10,  'MB', '+',
                    function(button, modifier)
                        stack:changeLayer(1)
                        button.modifier = modifier
                    end))
    local id = panel:addWidget(PushButton.New(18, 5, 22, 10, 'MB', '-',
                               function(button, modifier)
                                   stack:changeLayer(-1)
                                   button.modifier = modifier
                               end)).id

    local function changePage(page)
        if page ~= stack.page then
            panel.widgets[id+stack:changePage(page)].modifier = 'L'
        end
    end

    local function getHelp(screen, button)
        button.modifier = 'L'
        screen.panels = screen.helpPanels
        screen.svgFormat = screen.helpStyle
        screen.helpPage  = 1
    end

    panel:addWidget(PushButton.New(8, 12, 13, 17, 'MB', 'p1',
                    function() changePage(1) end)).modifier = 'S'
    panel:addWidget(PushButton.New(14,12, 19, 17, 'MB', 'p2',
                    function() changePage(2) end))
    panel:addWidget(PushButton.New(8, 18, 13, 23, 'MB', 'p3',
                    function() changePage(3) end))
    panel:addWidget(PushButton.New(14,18, 19, 23, 'MB', 'p4',
                    function() changePage(4) end))

    panel:addWidget(PushButton.New(9, 30, 18, 35, '',
                                   pushLabel(stack.undoStack, 'Undo'),
                                   function() stack:undo() end))
    panel:addWidget(PushButton.New(9, 37, 18, 42, '',
                                   pushLabel(stack.redoStack, 'Redo'),
                                   function() stack:redo() end))
    panel:addWidget(PushButton.New(9, 44, 18, 49, 'MB', 'Clear',
                    function() stack:clear() end))

    panel:addWidget(PushButton.New(5, 85, 22, 91, 'MB', 'Help',
                    function(btn) getHelp(self,btn) end))
    panel:addWidget(PushButton.New(5, 93, 22, 99, 'MB', 'Logout',
                    function() self:logout() end))
    table.insert(self.layerPanels, panel) -- right
end

function Screen:makePrinterPanels()
    local stack     = self.stack
    local panelSize = stack.DisplaySize
    local sideWidth = (AspectRatio-1)*panelSize/2
    local printer   = self.printer

    local layerNumFormat =
        function() return string.format('Layer: %s', self.stack.layerCursor) end
    local pageNumFormat =
        function()
            local page = self.printer:page()
            return string.format('Page: %s', page)
        end
    local panel     = Panel.New(0, sideWidth, panelSize, 24, 100)
    printer.layerLabel = PushButton.New(1,  5, 23, 10, 'MB',layerNumFormat)
    printer.pageLabel  = PushButton.New(1, 12, 23, 17, 'MB',pageNumFormat)
    panel:addWidget(printer.layerLabel)
    panel:addWidget(printer.pageLabel)
    panel:addWidget(PushButton.New(5, 47, 22, 52, 'MB', 'Cancel',
                    function() self:endPrint() end))
    panel:addWidget(PushButton.New(5, 93, 22, 99, 'MB', 'Logout',
                    function() self:logout() end))
    table.insert(self.printerPanels, panel)
    panel = Panel.New(sideWidth, panelSize, panelSize, panelSize, panelSize)
    panel:addWidget(printer)
    table.insert(self.printerPanels, panel)
end

function Screen:scaleXY(x, y)
    return AspectRatio*self.stack.DisplaySize*x, self.stack.DisplaySize*y
end

function Screen:updatePosition(x, y, leftHeld)
    if self.stack.vertexEditor then
        x, y = self:scaleXY(x, y)
        x    = x - self.layerPanels[2].xoffset
        local shared = self.stack.vertexEditor[1]
        local origin = self.stack.vertexEditor[3]
        shared[1] = Stack.AlignedPosition(x, origin[1])
        shared[2] = Stack.AlignedPosition(y, origin[2])
        return true
    end
    if leftHeld then
        x, y = self:scaleXY(x, y)
        x    = x - self.layerPanels[2].xoffset
        self.stack:setDisplayCoords({x,y})
        return true
    end
    return self.stack:setDisplayCoords()
end

return Screen
