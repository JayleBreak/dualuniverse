local PlaceOrigin     = "14xm26xm7" --export: Construct Coords of upper left corner
local PickOrigin      = "4x27x4" --export: Construct Coords of board center
local ColumnDirection = "mx"   --export: Direction for increasing columns p|m.x|y
local deltas = {['mx']={-.25,0}, ['px']={.25,0},
                ['my']={0,-.25}, ['py']={0, .25}}

local rots   = {['mx']={0,180,0},  ['px']={0,180,0},
                ['my']={180,0,90}, ['py']={180,0,90}}

Printer         = {}
Printer.__index = Printer
Printer.PlaceOrigin = {}
Printer.PickOrigin  = {}
Printer.ColumnDelta = deltas[ColumnDirection]
Printer.Rotation    = rots[ColumnDirection]

for _, num in ipairs({PlaceOrigin:match('(m?%d+)x(m?%d+)x(m?%d+)')}) do
    num = num:gsub('m', '-')
    table.insert(Printer.PlaceOrigin, tonumber(num)/4) -- in meters
end

for _, num in ipairs({PickOrigin:match('(m?%d+)x(m?%d+)x(m?%d+)')}) do
    num = num:gsub('m', '-')
    table.insert(Printer.PickOrigin, tonumber(num)/4) -- in meters
end

local function needsToBePrinted(row, col, layer)
    local LayerSz = layer.LayerSz
    local rows    = layer.rows

    if rows[row][col]:hasMaterial() then return true end

    if row == 1 then
        return col < LayerSz and rows[row][col+1]:hasMaterial()
    end

    if col == LayerSz then return rows[row-1][col]:hasMaterial() end

    return rows[row-1][col  ]:hasMaterial() or
           rows[row-1][col+1]:hasMaterial() or
           rows[row]  [col+1]:hasMaterial()
end

function Printer.New(svgFn)
    local printer = setmetatable({active = false, PolygonSvg = svgFn}, Printer)
    return printer
end

function Printer:draw()
    local svgs     = {}
    local active   = self.layer
    local row      = self.row
    local col      = self.col
    local voxel    = active.rows[row][col]
    local page     = voxel:page()
    local polygons = active:polygons(page, true)
    local polygon  = voxel:polygon(page)

    self.PolygonSvg(svgs, active:polygons(page, true), 'L')
    
    if polygon then
        self.PolygonSvg(svgs, {{polygon, voxel}}, 'S')
    end
    local startRow, _, startCol = table.unpack(active.PageRange[page])
    local x        = (col - startCol + .5)*voxel.VoxelSz + voxel.Offset
    local y        = (row - startRow + .5)*voxel.VoxelSz + voxel.Offset
    table.insert(svgs, string.format([[
<circle cx="%.1f" cy="%.1f" r="%.1f" fill="blue"/>]], x, y, voxel.VoxelSz/3))
    return table.concat(svgs,'\n')
end

function Printer.onClick()
end

function Printer:endPrint()
    self.active = false
    -- destroy stickers
    if self.placeArrow then
        _core.deleteSticker(self.placeArrow)
        self.placeArrow = nil
    end
    if self.placeNumber then
        _core.deleteSticker(self.placeNumber)
        self.placeNumber = nil
    end
    if self.pickArrow then
        _core.deleteSticker(self.pickArrow)
        self.pickArrow = nil
    end
end

function Printer:placeSticker(row, col)
    local x, y, z = table.unpack(Printer.PlaceOrigin)
    z = z - (row-1)*0.25
    x = x + (col-2)*Printer.ColumnDelta[1]
    y = y + (col-2)*Printer.ColumnDelta[2]

    if self.placeArrow then
        _core.moveSticker(self.placeArrow, x, y, z)
    else
        self.placeArrow = _core.spawnArrowSticker(x, y, z, "up")
        if self.placeArrow < 0 then
            self.placeArrow = nil
            self.active = false
            printf('Failed to spawn place sticker')
            return
        end
        _core.rotateSticker(self.placeArrow, table.unpack(Printer.Rotation))
    end
    local voxel    = self.layer.rows[row][col]
    local material = voxel:getMaterial()

    if self.numberSticker ~= material then
        if self.placeNumber then
            _core.deleteSticker(self.placeNumber)
        end
        self.placeNumber = _core.spawnNumberSticker(material,x,y,z+.25,"front")
        if self.placeNumber < 0 then
            self.placeNumber = nil
            self.active = false
            printf('Failed to spawn number sticker')
            return
        end
        _core.rotateSticker(self.placeNumber, 0, 0, Printer.Rotation[3])
    else
        _core.moveSticker(self.placeNumber, x, y, z+.25)
    end
    col, row = table.unpack(voxel:getUpperRight())
    x, y, z  = table.unpack(Printer.PickOrigin)
    z = z - row/2 + .125
    x = x + 2*col*Printer.ColumnDelta[1]
    y = y + 2*col*Printer.ColumnDelta[2]

    if self.pickArrow then
        _core.moveSticker(self.pickArrow, x, y, z)
    else
        self.pickArrow = _core.spawnArrowSticker(x, y, z, "up")
        if self.pickArrow < 0 then
            self.pickArrow = nil
            self.active = false
            printf('Failed to spawn place sticker')
            return
        end
        _core.rotateSticker(self.pickArrow, table.unpack(Printer.Rotation))
    end
end

function Printer:init(layer, onInactive)
    self.active     = true
    self.onInactive = onInactive
    self.row        = layer.LayerSz
    self.col        = 1
    self.layer      = layer
    if not needsToBePrinted(self.row, self.col, layer) then
        self:nextVoxel()

        if not self.active then return false end
    end
    self:placeSticker(self.row, self.col)
    -- create stickers
    return self.active
end

function Printer:nextVoxel()
    if self.active then
        local size     = self.layer.LayerSz
        local row, col = self.row, self.col
        repeat
            col = col + 1
            if col > size then
                col = 1
                row = row - 1
            end
        until row < 1 or needsToBePrinted(row, col, self.layer)
        self.active = row >= 1
        self.row    = row -- always -z
        self.col    = col -- either +/-x, +/-y

        if self.active then
            self:placeSticker(row, col)
        else
            self.onInactive()
        end
    end
    return self.active
end

function Printer:page()
    return self.layer.rows[self.row][self.col]:page()
end

return Printer
