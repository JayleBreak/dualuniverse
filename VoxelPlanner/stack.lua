local PAGESZ  = 8
local STACK   = 12
local LAYERSZ = 2*PAGESZ
local SIDE    = 8
local OFFSET  = 12
local MAX_XY  = 2*OFFSET + PAGESZ*SIDE
local MAXUSERS= 3 -- per databank: (3*16*16+2)*12 = 9240 bytes per user
local DSPSZ   = MAX_XY + 2
local ASCII_A = string.byte('A')
local ASCII_0 = string.byte('0')

local utils = require('cpml.utils')
local round = utils.round

local pageRange = { {1,        PAGESZ, 2,       PAGESZ+1},
                    {1,        PAGESZ, PAGESZ+1,LAYERSZ },
                    {PAGESZ,LAYERSZ-1, 2,       PAGESZ+1},
                    {PAGESZ,LAYERSZ-1, PAGESZ+1,LAYERSZ } }

local function distance(p1, p2)
    return math.sqrt((p1[1]-p2[1])^2 + (p1[2]-p2[2])^2)
end

local function closestVertex(vertices, x, y)
    local point       = {x,y}
    local closest     = 1
    local minDistance = distance(point, vertices[1])

    for i=2,#vertices do
        local d = distance(point, vertices[i])
        if d < minDistance then
            minDistance = d
            closest     = i
        end
    end
    return closest
end

local function textCoords(x, y, w, urx, ury)
    return string.format([[
<text x="%.1f" y="%.1f" class="T" font-size="%.1f" dominant-baseline="middle" text-anchor="middle">%.0f,%.0f</text>]],
        x+w/2, y, SIDE/3, urx, ury)
end

local function transformFrom(point, startRow, startCol)
    point[1] = point[1] - OFFSET + (startCol-2)*SIDE
    point[2] = point[2] - OFFSET + (startRow-1)*SIDE
end

local function transformTo(point, startRow, startCol)
    point[1] = point[1] + OFFSET - (startCol-2)*SIDE
    point[2] = point[2] + OFFSET - (startRow-1)*SIDE
end


local Voxel   = { VoxelSz = SIDE, Offset = OFFSET }
Voxel.__index = Voxel

function Voxel.New(layer, row, column, layerInfo)
    -- neighbors are left, below-left, and below
    -- "scaffold" voxels may be missing all of these
    return setmetatable({layer         = layer,
                         rowIndex      = row,
                         columnIndex   = column,
                         layerInfo     = layerInfo},
                        Voxel)
end

function Voxel.CalcPage(row, col)
    if row <= PAGESZ then
        return col <= PAGESZ and 1 or 2
    end
    return col <= PAGESZ and 3 or 4
end

function Voxel:encodeData()
    if self.urx then
        return string.char(ASCII_0+self.material,
                           ASCII_A+(self.urx+OFFSET),
                           ASCII_A+(self.ury+OFFSET))
    end
    local index = 3*((self.rowIndex-1)*LAYERSZ + self.columnIndex-1) + 1
    return self.layerInfo.data:sub(index, index+2)
end

function Voxel:getData()
    local index        = 3*((self.rowIndex-1)*LAYERSZ + self.columnIndex-1) + 1
    local b1, b2, b3   = self.layerInfo.data:byte(index, index+2)
    self.urx, self.ury = b2-OFFSET-ASCII_A, b3-OFFSET-ASCII_A
    self.material      = b1-ASCII_0
end

function Voxel:getMaterial()
    if not self.urx then self:getData() end
    return self.material
end

function Voxel:getUpperRight()
    if not self.urx then self:getData() end
    return {self.urx, self.ury} -- -12 to +12
end

function Voxel:hasMaterial()
    if not self.urx then self:getData() end
    return self.material > 0
end

function Voxel:owner(verticeIndex)
    if verticeIndex == 2 then return self end
    if verticeIndex == 4 then return self.neighbors[2] end
    return self.neighbors[verticeIndex]
end

function Voxel:page()
    if not self.pageNum then
        self.pageNum = Voxel.CalcPage(self.rowIndex, self.columnIndex)
    end
    return self.pageNum
end

function Voxel:polygon(pageNum)
    local page = self:page()
    -- vertices that form page borders can have two different coordinates
    -- depending on which page they are displayed on.
    local n1, n2, n3   = table.unpack(self.neighbors)
    if n1 and n2 and n3 then
        local vertices     = { n1:vertexCoord(),
                               self:vertexCoord(),
                               n3:vertexCoord(),
                               n2:vertexCoord() }

        if pageNum then
            local range                              = pageRange[pageNum]
            local startRow, endRow, startCol, endCol = table.unpack(range)

            if  self.rowIndex    < startRow or self.rowIndex    > endRow or
                self.columnIndex < startCol or self.columnIndex > endCol then
                return nil
            end

            for _,v in ipairs(vertices) do
                transformTo(v, startRow, startCol)
            end
        end
        return vertices
    end
    return nil
end

function Voxel:setMaterial(newValue)
    if not self.material then self:getData() end
    self.material        = newValue
    self.layerInfo.dirty = true
end

function Voxel:setUpperRight(x, y)
    if not self.urx then self:getData() end
    if type(x) == 'table' then
        self.urx, self.ury = table.unpack(x)
    else
        if x then self.urx = x end
        if y then self.ury = y end
    end
    self.layerInfo.dirty = true
end

function Voxel:vertexCoord()
    if not self.urx then self:getData() end
    local x = (self.columnIndex-1)*SIDE
    local y = (self.rowIndex   -1)*SIDE
    return {x + self.urx, y + self.ury}
end

-- From "Inclusion of a Point in a Polygon" by Dan Sunday
local function isLeft(p0, p1, p2)
    return ((p1[1] - p0[1]) * (p2[2] - p0[2]) -
             (p2[1] - p0[1]) * (p1[2] - p0[2]))
end

local function wnPnPoly(p, v)
    local wn = 0

    for i=1,4 do
        if v[i][2] <= p[2] then
            if v[i+1][2] > p[2] and isLeft(v[i], v[i+1], p) > 0 then
                wn = wn + 1
            end
        elseif v[i+1][2] <= p[2] then
            if isLeft(v[i], v[i+1], p) < 0 then wn = wn - 1 end;
        end
    end
    return wn > 0;
end

local Layer = { Size = LAYERSZ, PageRange = pageRange }
Layer.__index = Layer

function Layer.New(layer, data)
    local rows     = {}
    local DataSz   = LAYERSZ*LAYERSZ

    if not data or #data ~= 3*DataSz then
        data = string.char(ASCII_0,ASCII_A+OFFSET,ASCII_A+OFFSET):rep(DataSz)
    end
    local newLayer = setmetatable({layer   = layer,
                                   rows    = rows,
                                   data    = data,
                                   dirty   = false,
                                   LayerSz = LAYERSZ}, Layer)
    for row = 1,LAYERSZ do
        local currentRow = {}

        for col = 1,LAYERSZ do
            table.insert(currentRow, Voxel.New(layer, row, col, newLayer))
        end
        table.insert(rows, currentRow)
    end

    for row = 1,LAYERSZ-1 do
        local currentRow = rows[row]
        local nextRow    = rows[row+1]

        currentRow[1].neighbors = { nil, nil, nextRow[1] }

        for col = 2,LAYERSZ do
            currentRow[col].neighbors = { currentRow[col-1], -- upper left
                                          nextRow[col-1],    -- lower left
                                          nextRow[col] }     -- lower right
        end
    end
    local currentRow        = rows[LAYERSZ]
    currentRow[1].neighbors = {nil, nil, nil}

    for col = 2,LAYERSZ do
        currentRow[col].neighbors = { currentRow[col-1], nil, nil }
    end
    return newLayer
end

function Layer:findVoxel(page, point)
    -- Painting order is top-left to bottom right. Search order is opposite:
    local unused
    local startRow, endRow, startCol, endCol=table.unpack(pageRange[page])
    transformFrom(point, startRow, startCol)

    for row = endRow, startRow, -1 do
        for col = endCol, startCol, -1 do
            local voxel = self.rows[row][col]
            local v     = voxel:polygon()

            table.insert(v, v[1])

            if wnPnPoly(point, v) then
                if voxel:hasMaterial() then
                    return voxel, scaffold
                end
                unused = voxel
            end
        end
    end
    return nil, unused
end

function Layer:okToResetVertex(owner)
    if owner:hasMaterial() then return false end

    local ownerRow, ownerColumn = owner.rowIndex, owner.columnIndex
    local rows = self.rows

    if ownerRow > 1 then
        if rows[ownerRow-1][ownerColumn]:hasMaterial() then return false end

        if ownerColumn < LAYERSZ then
           if rows[ownerRow-1][ownerColumn+1]:hasMaterial() or
              rows[ownerRow][ownerColumn+1]:hasMaterial()   then
               return false
           end
        end
    elseif ownerColumn < LAYERSZ then
        if rows[ownerRow][ownerColumn+1]:hasMaterial() then return false end
    end
    return true
end

function Layer:polygons(pageNum, onlyNonNilMaterial) -- 1-4
    local polygons = {}
    local startRow, endRow, startCol, endCol = table.unpack(pageRange[pageNum])

    for row = startRow, endRow do
        for col = startCol, endCol do
            local voxel    = self.rows[row][col]
            local vertices = voxel:polygon(pageNum)

            if not onlyNonNilMaterial or voxel:hasMaterial() then
                table.insert(polygons, {vertices, voxel})
            end
        end
    end
    return polygons
end

function Layer:sharedVertex(owner, pageNum)
    local ownerRow, ownerColumn = owner.rowIndex, owner.columnIndex
    local startRow, _, startCol, _ = table.unpack(pageRange[pageNum])
    local polygons  = {}

    -- following assumes vox:polygon implementation
    local polygon   = owner:polygon(pageNum)
    local shared

    if polygon then
        shared = polygon[2]
        table.insert(polygons, {polygon, owner})
    else
        shared = owner:vertexCoord()
        transformTo(shared, startRow, startCol)
    end

    if ownerRow > 1 then
        local voxel = self.rows[ownerRow-1][ownerColumn]
        polygon     = voxel:polygon(pageNum)
        
        if polygon then
            polygon[3]  = shared
            table.insert(polygons, {polygon, voxel})
        end
    end

    if ownerColumn < LAYERSZ then
        local voxel     = self.rows[ownerRow][ownerColumn+1]
        polygon         = voxel:polygon(pageNum)

        if polygon then
            polygon[1]  = shared
            table.insert(polygons, {polygon, voxel})
        end

        if ownerRow > 1 then
            voxel       = self.rows[ownerRow-1][ownerColumn+1]
            polygon     = voxel:polygon(pageNum)

            if polygon then
                polygon[4]  = shared
                table.insert(polygons, {polygon, voxel})
            end
        end
    end
    return shared, polygons
end

local Stack   = {
    Depth       = STACK,
    PageSz      = PAGESZ,
    VoxelSz     = SIDE,
    Offset      = OFFSET,
    DisplaySize = DSPSZ,
}

Stack.__index = Stack

function Stack.AlignedPosition(pos, origin)
    local diff = pos - origin
    if diff > OFFSET then
        return OFFSET + origin
    end

    if diff < -OFFSET then
        return -OFFSET + origin
    end
    return round(diff) + origin
end

function Stack.New(PolygonSvg, uid, dbArray)
    local layers         = {}
    local uids           = tostring(uid)
    local maxUsers       = MAXUSERS*#dbArray
    local db             = dbArray[1]
    local users          = db.getStringValue('users')
    local userList       = {}
    local prefix

    if users then
        for s,l in users:gmatch('(%d+)(%L)') do
            if s == uids then
                prefix = l
            else
                table.insert(userList, s .. l)
            end
        end
    end

    if not prefix then -- Removing a databank requires all databanks be cleared
        if #userList >= maxUsers then
            local drop = table.remove(userList)
            local user
            user, prefix = drop:match('(%d+)(%L)')

            for i=1,STACK do
                local key = prefix .. string.char(64+i)
                if db.hasKey(key) then
                    db.setStringValue(key,'')
                end
            end
            printf('Deleted %s to accept new user.', user)
        else
            prefix = string.char(ASCII_A+#userList)
        end
    end
    table.insert(userList, 1, uids .. prefix)
    users = table.concat(userList, ',')
    db.setStringValue('users', users)

    printf("users: %s", users)
    printf("uid:   %s (%s)", uid, prefix)

    local dbIndex = 1+math.floor((prefix:byte()-ASCII_A)/MAXUSERS)
    db            = dbArray[dbIndex]
    local layer1  = db.getStringValue(prefix .. 'A')
    table.insert(layers, Layer.New(1, layer1))

    for i = 2, Stack.Depth do
        table.insert(layers, nil)
    end
    return setmetatable({PolygonSvg  = PolygonSvg,
                         layerCursor = 1,
                         page        = 1,
                         layers      = layers,
                         db          = db,
                         dbArray     = dbArray,
                         uid         = uid,
                         prefix      = string.byte(prefix),
                         redoStack   = {},
                         undoStack   = {}}, Stack)
end

function Stack:changeLayer(delta, original)
    self:saveData()
    local new = self.layerCursor + delta
    if     new < 1            then self.layerCursor = Stack.Depth
    elseif new > Stack.Depth  then self.layerCursor = 1
    else                           self.layerCursor = new
    end
    return self.layerCursor
end

function Stack:changeMaterial(materialIndex)
    local previous       = self.currentMaterial
    self.currentMaterial = materialIndex
    return previous
end

function Stack:changePage(page)
    self:saveData()
    local prev = self.page
    if page >= 1 and page <= 4 then
        self.page = page
    end
    return prev
end

function Stack:clear()
    local index    = self.layerCursor
    local page     = self.page
    local newLayer = Layer.New(index)
    local oldLayer = self.layers[index]
    local fn2 = {}
    table.insert(fn2,   function() 
                            self.layerCursor   = index
                            self.page          = page
                            self.layers[index] = newLayer
                            return
                                function()
                                    self.layerCursor   = index
                                    self.page          = page
                                    self.layers[index] = oldLayer
                                    return fn2[1]
                                end
                        end)
    table.insert(self.undoStack, fn2[1]())
    self.redoStack    = {}
end

function Stack:cursor()
    return self.layerCursor
end

function Stack:deleteAll()
    self:reset()
    for _, db in ipairs(self.dbArray) do
        db.clear()
    end
    printf('All databanks reset')
end

function Stack:draw()
    local polySVGs = {}
    local below    = self:layer(self.layerCursor-1)
    local active   = self:layer(self.layerCursor)
    if below then
        self.PolygonSvg(polySVGs, below:polygons(self.page), 'W')
    end

    if self.vertexEditor then
        if not below then
            self.PolygonSvg(polySVGs, active:polygons(self.page), 'W')
        end
        self.PolygonSvg(polySVGs, self.vertexEditor[2], 'S', 0)
        local shared, _, origin, bottom  = table.unpack(self.vertexEditor)
        local x, y    = origin[1]-self.Offset, origin[2]-self.Offset
        local w, h    = 2*self.Offset, 2*self.Offset
        local editBox = string.format([[
<rect x="%s" y="%s" width="%s" height="%s" fill="#ffffff" fill-opacity=".2"/>]],
            x, y, w, h)
        table.insert(polySVGs, editBox)
        if bottom then
            y = y-self.VoxelSz/4
        else
            y = y+h+self.VoxelSz/4
        end
        table.insert(polySVGs,
                 textCoords(x, y, w, shared[1]-origin[1], shared[2]-origin[2]))
    else
        self.PolygonSvg(polySVGs, active:polygons(self.page), 'L')

        if self.displayCoords then
            local x, y = table.unpack(self.displayCoords)
            local urx  = (x-OFFSET)%SIDE - SIDE;
            local ury  = (y-OFFSET)%SIDE;
            table.insert(polySVGs,
                     textCoords(x, y+1, 0, urx == -8 and 0 or urx, ury))
        end
    end
    return table.concat(polySVGs,'\n')
end

function Stack:endEditFn(x, y, owner, shared, polygons, bottom)
    local oldURx, oldURy = table.unpack(owner:getUpperRight())
    local origin         = { shared[1] - oldURx, shared[2] - oldURy }
    shared[1] = Stack.AlignedPosition(x, origin[1])
    shared[2] = Stack.AlignedPosition(y, origin[2])

    self.vertexEditor = { shared, polygons, origin, bottom }

    return
        function(down, button, x, y)
            if down or button ~= 3 or x < -4 or x > DSPSZ+4
            then
                owner:setUpperRight(oldURx, oldURy)
                self.vertexEditor = nil
                return -- canceled
            end
            local layer = self.layerCursor
            local page  = self.page
            local new = { Stack.AlignedPosition(x, origin[1]) - origin[1],
                          Stack.AlignedPosition(y, origin[2]) - origin[2] }
            local fn2 = {}
            table.insert(fn2,   function() 
                                    owner:setUpperRight(new)
                                    self.layerCursor = layer
                                    self.page        = page
                                    return
                                        function()
                                            owner:setUpperRight(oldURx, oldURy)
                                            self.layerCursor = layer
                                            self.page        = page
                                            return fn2[1]
                                        end
                                end)
            table.insert(self.undoStack, fn2[1]())
            self.redoStack    = {}
            self.vertexEditor = nil
        end
end

function Stack:layer(index)
    index = index or self.layerCursor
    if index >= 1 and index <= Stack.Depth then
        local layer = self.layers[index]
        if not layer then
            local data = self:layerString(index)
            layer = Layer.New(index, data)
            self.layers[index] = layer
        end
        return layer
    end
    return nil
end

function Stack:layerString(index)
    index = index or self.layerCursor
    return self.db.getStringValue(string.char(self.prefix, index+ASCII_A-1))
end

function Stack:onClick(button, x, y)
    local layer           = self.layers[self.layerCursor]
    local setVox,unsetVox = layer:findVoxel(self.page, {x,y})
    local voxel           = setVox or unsetVox

    if not voxel then
        return function() end
    end
    if button == 1 then -- LMB
        return function(down, button, x, y)
                if not down and button == 1 then
                    local row1, col1 = voxel.rowIndex, voxel.columnIndex
                    setVox,unsetVox  = layer:findVoxel(self.page, {x,y})
                    voxel            = setVox or unsetVox
                    if voxel then
                        local row2, col2 = voxel.rowIndex, voxel.columnIndex
                        if row1 > row2 then row1, row2 = row2, row1 end
                        if col1 > col2 then col1, col2 = col2, col1 end
                        local list = {}

                        for i=1,(row2-row1+1)*(col2-col1+1) do
                            table.insert(list, self.currentMaterial)
                        end
                        local capture = {self.layerCursor, self.page,
                                         row1, row2, col1, col2, list}
                        local fn2 = {}
                        table.insert(fn2,
                            function() 
                                self:swapRange(capture)
                                return
                                    function()
                                        self:swapRange(capture)
                                        return fn2[1]
                                    end
                            end)
                        table.insert(self.undoStack, fn2[1]())
                        self.redoStack = {}
                    end
                end
            end
    end

    if button == 2 then -- Alt+LMB
        if setVox then
            return
                function(down, button, x, y)
                    if not down and button == 2 then
                        local v     = setVox:polygon(self.page)
                        table.insert(v, v[1])

                        if wnPnPoly({x,y}, v) then
                            local fn2 = self:resetVox(setVox)
                            table.insert(self.undoStack, fn2())
                            self.redoStack = {}
                        end
                    end
                end
            
        end
    elseif button == 3 then -- Shift+LMB
        local vertices         = voxel:polygon(self.page)
        local closest          = closestVertex(vertices, x, y)
        local layer            = self.layers[self.layerCursor]
        local page             = self.page
        local owner            = voxel:owner(closest)
        local shared, polygons = layer:sharedVertex(owner, page)
        local bottom = (page < 3 and owner.rowIndex == PAGESZ+1) or
                       (page > 2 and owner.rowIndex == LAYERSZ)

        for _, poly in ipairs(polygons) do -- ignore unless one has material set
            local voxel = poly[2]
            if poly[2]:hasMaterial() then
                return self:endEditFn(x, y, owner, shared, polygons, bottom)
            end
        end
        self.vertexEditor = nil
    end
    return function() end -- otherwise do nothing.
end

function Stack:redo()
    if #self.redoStack > 0 then
        local fn = table.remove(self.redoStack)
        table.insert(self.undoStack, fn())
    end
end

function Stack:reset()
    for i=1,STACK do
        local key = string.char(self.prefix, 64+i)
        if self.db.hasKey(key) then
            self.db.setStringValue(key,'')
        end
    end
    self.layerCursor = 1
    self.page        = 1
    self.redoStack   = {}
    self.undoStack   = {}
    self.layers      = {}
    table.insert(self.layers, Layer.New(1))
end

function Stack:resetVox(voxel)
    local material  = voxel:getMaterial()
    local layer     = self.layers[voxel.layer]
    local resetList = {}

    voxel:setMaterial(0)

    if layer:okToResetVertex(voxel) then
        table.insert(resetList, {voxel, voxel:getUpperRight()})
    end

    for _,n in ipairs(voxel.neighbors) do
        if n and layer:okToResetVertex(n) then
            table.insert(resetList, {n, n:getUpperRight()})
        end
    end
    local layer = self.layerCursor
    local page  = self.page
    local fn2   = {}
    table.insert(fn2,   function()
                            voxel:setMaterial(0)
                            for _,n in ipairs(resetList) do
                                n[1]:setUpperRight(0,0)
                            end
                            self.layerCursor = layer
                            self.page        = page
                            return
                                function()
                                    voxel:setMaterial(material)
                                    for _,n in ipairs(resetList) do
                                        n[1]:setUpperRight(n[2])
                                    end
                                    self.layerCursor = layer
                                    self.page        = page
                                    return fn2[1]
                                end
                        end)
    return fn2[1]
end

function Stack:saveData()
    local layer = self.layers[self.layerCursor]
    local rows  = layer.rows
    if layer.dirty then
        local voxels = {}
        for row = 1,LAYERSZ do
            for col = 1,LAYERSZ do
                table.insert(voxels, rows[row][col]:encodeData())
            end
        end
        local data = table.concat(voxels)
        self.db.setStringValue(string.char(self.prefix, 64+self.layerCursor),
                               data)
        layer.dirty = false
    end
end

function Stack:setDisplayCoords(coords)
    local prev = self.displayCoords
    self.displayCoords = coords
    return prev
end

function Stack:swapRange(info)
    local index, page, row1, row2, col1, col2, list = table.unpack(info)
    self.layerCursor = index
    self.page        = page

    local layer      = self.layers[index]
    local colSz      = col2-col1+1
    local material, voxel

    for r=row1, row2 do
        for c=col1, col2 do
            index = (r-row1)*colSz + (c-col1) + 1
            material, voxel = list[index], layer.rows[r][c]
            list[index]     = voxel:getMaterial()
            voxel:setMaterial(material)
        end
    end
end

function Stack:undo()
    if #self.undoStack > 0 then
        local fn = table.remove(self.undoStack)
        table.insert(self.redoStack, fn())
        assert(#self.redoStack > 0)
    end
end

return Stack
